This project aims to simplify reconfiguring embedded home automation devices after you hide them in your home.  
With the c / arduino based projects you need to recompile and upload the new firmware, with micropython your runtime stays the same and you just upload the new code to your embedded device.


This is a work in progress and at the moment more of an inspiration for your own experiments.

How to use:
 - setup a microcontroler with [micropython](https://micropython.org/download)
 - install mpfshell (if you have trouble connecting try downgrading the websocket module)
 - edit `boot.py` and fill in your wifi credentials.
 - make a private copy of the wifi config `cp wifi-config.json.sample wifi-config.json`, then edit `wifi-config.json` and enter your wifi credentials
 - edit `config.json` and configure your mqtt host, topic and the pin you want as an output.
 - edit `webrepl_cfg.py` change password to a 4 - 8 char password, I had issues with complex passwords.
 - open mpfshell over local serial and upload the files
 - now you can reach it over the network after restarting your microcontroler
