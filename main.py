from umqtt.simple import MQTTClient
from machine import Pin
import ubinascii
import machine
import micropython
import json

with open('config.json') as json_data_file:
    cfg = json.load(json_data_file)


led = Pin(int(cfg['output']['pin']), Pin.OUT, value=0)

# Default MQTT server to connect to
SERVER = cfg['mqtt']['host']
CLIENT_ID = ubinascii.hexlify(machine.unique_id())
TOPIC = str.encode(cfg['mqtt']['topic'])


def sub_cb(topic, msg):
    # print(msg)
    try:
        state = json.loads(msg)['state']
        if state == "ON":
            led.value(1)
        elif state == "OFF":
            led.value(0)
        elif state == "TOGGLE":
            led.value(not led.value())
    except:
        print('Invalid json or payload')

def main(server=SERVER):
    c = MQTTClient(CLIENT_ID, server)
    # Subscribed messages will be delivered to this callback
    c.set_callback(sub_cb)
    c.connect()
    c.subscribe(TOPIC)
    print("Connected to %s, subscribed to %s topic" % (server, TOPIC))

    try:
        while 1:
            #micropython.mem_info()
            c.wait_msg()
    finally:
        c.disconnect()

main()
